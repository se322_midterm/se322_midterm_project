package app;

public class TenThousandExpresion extends Expression{
    @Override
    public String One() {
        return "K";
    }

    @Override
    public String Four() {
        return "KJ";
    }

    @Override
    public String Five() {
        return "J";
    }

    @Override
    public String Nine() {
        return "KG";
    }

    @Override
    public int Multiplier() {
        return 10000;
    }
}
