package app;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Interpreter Demo");
        //M = 1000
        //B = 5000
        //K = 10,000
        //J = 50,000
        //G = 100,000
        //R = 500,000
        //S = 1,000,000
        //O = 5,000,000
        //SP = 9,000,000

        //You can remove comment each one to test out the Interpreter
        String roman = "SPGSKGMKCMXCIX";
        //String roman = "ORJBDLV";
        //String roman = "SOGRKJMBCDXLIV";
        //String roman = "SPGKMCXI";
        Context context = new Context(roman);

        List<Expression> tree = new ArrayList<Expression>();
        tree.add(new MillionExpression());
        tree.add(new HundrendThousandExpression());
        tree.add(new TenThousandExpresion());
        tree.add(new ThousandExpression());      
        tree.add(new HundredExpression());
        tree.add(new TenExpression());
        tree.add(new OneExpression());

        for(int i = 0;i<roman.length();i++){
            for(Expression e : tree){
                e.Interpret(context);

            }
        }
        System.out.println(roman + " = " + context.getIoutput());

    }
}