package app;

public class MillionExpression extends Expression {
    @Override
    public String One() {
        return "S";
    }

    @Override
    public String Four() {
        return "SO";
    }

    @Override
    public String Five() {
        return "O";
    }

    @Override
    public String Nine() {
        return "SP";
    }

    @Override
    public int Multiplier() {
        return 1000000;
    }
}
