package app;

public class HundrendThousandExpression extends Expression {
    @Override
    public String One() {
        return "G";
    }

    @Override
    public String Four() {
        return "GR";
    }

    @Override
    public String Five() {
        return "R";
    }

    @Override
    public String Nine() {
        return "GS";
    }

    @Override
    public int Multiplier() {
        return 100000;
    }
}
