package app;

public class ThousandExpression extends Expression{

    @Override
    public String One() {
        return "M";
    }

    @Override
    public String Four() {
        return "MB";
    }

    @Override
    public String Five() {
        return "B";
    }

    @Override
    public String Nine() {
        return "MK";
    }

    @Override
    public int Multiplier() {
        return 1000;
    }

}