import java.awt.*;

public class MyRectFactory {
    private Color color = Color.black;

    public MyRectFactory(Color color) {
        this.color = color;
    }
    public void draw(Graphics g, int upperX, int upperY, int lowerX, int lowerY) {
        g.setColor(color);
        g.fillRect(upperX, upperY, lowerX, lowerY);
    }
}